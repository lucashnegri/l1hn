#ifndef L1HN_AMP
#define L1HN_AMP

/* Implements the first order approximate message passing [1] algorithm.
 * [1]: D. L. Donoho, A. Maleki, and A. Montanari - Message-passing algorithms for compressed sensing, 2009
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include <vector>
#include <algorithm>
#include <cmath>

namespace l1hn
{
    /* soft threshold, modifies _x_. */
    inline void soft_thres(Eigen::VectorXd& x, double lambda)
    {
        for(int i = 0, e = x.rows(); i < e; ++i)
        {
            if(x[i] > lambda)
            {
                x[i] -= lambda;
                continue;
            }
            
            if(x[i] < -lambda)
            {
                x[i] += lambda;
                continue;    
            }
            
            x[i] = 0;
        }
    }
    
    /* soft threshold derivative d/dx, modifies _x_. */
    inline void soft_thres_dx(Eigen::VectorXd& x, double lambda)
    {
        for(int i = 0, e = x.rows(); i < e; ++i)
        {
            if(x[i] > lambda || x[i] < -lambda)
                x[i] = 1;
            else
                x[i] = 0;
        }
    }
    
    /* Computes an appropriate threshold value.
     * Assuming that we can have at most _m_ non-zero measurements, we find the _m_-th
     * greater absolute value of the current input _x_ and use it as threshold. This will
     * force that not more than _m_ values are non-zero.
     */
    inline double find_threshold(const Eigen::VectorXd& x, int m)
    {
        std::vector<double> absVals;
        for(int i = 0, e = x.rows(); i < e; ++i)
            absVals.push_back(std::fabs(x[i]));
        
        std::partial_sort(absVals.begin(), absVals.begin()+m, absVals.end(), std::greater<double>());
        return 1.35*absVals[m-1];
    }
    
    /* 
     * First order approximate message passing (AMP).
     * Returns the reconstructed _x_ vector, given a measurement matrix _A_ and the measured
     * values _y_. Runs for at most _max\_iters_ or when a residue l2-norm less or equal to
     * _res\_limit_ is found.
     */
    inline Eigen::VectorXd amp(const Eigen::MatrixXd& A, const Eigen::VectorXd& y,
                                      int max_iters = 1e5, double res_limit = 1e-6)
    {
        int N = A.cols(), m = y.rows();
        
        Eigen::VectorXd x  = Eigen::VectorXd::Zero(N);
        Eigen::VectorXd z  = Eigen::VectorXd::Zero(m);
        double invDelta    = m / (double)N;

        Eigen::VectorXd gamma = Eigen::VectorXd::Zero(N);
        double thres = 0.0;
        
        for(int t = 0; t < max_iters; ++t)
        {
            const Eigen::VectorXd res = y - A*x;
            if(res.norm() < res_limit) break;
            
            // compute z
            soft_thres_dx(gamma, thres);
            const Eigen::VectorXd ZZ = invDelta*z*(gamma.mean());
            z  = res + ZZ;
            
            // compute new x
            gamma   = A.transpose()*z + x; // gamma is saved for the next iteration
            x       = gamma;
            thres   = find_threshold(x, y.rows());
            soft_thres(x, thres);
        }
        
        return x;
    }
};

#endif
