#ifndef L1HN_BP
#define L1HN_BP

/* Implements the basis pursuit algorithm by using lpw / lpsolve.
 * Only works for positive x.
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include <lpw>
#include <algorithm>

namespace l1hn
{
    inline Eigen::VectorXd bp(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
    {
        const int vars = A.cols();
        
        lpw::Solver solver(2*vars);
        lpw::VD row = solver.makeRow();
        
        // Ax = b
        for(int i = 0; i < A.rows(); ++i)
        {
            for(int j = 0; j < vars; ++j)
                row[j] = A(i,j);
            
            solver.addConstraint(row, lpw::EQ, b(i));
        }
        
        // (on the x only, t is positive only)
        for(int i = 0; i < vars; ++i)
            solver.setBounds(i, -100.0, 100.0);
        
        std::fill(row.begin(), row.end(), 0);
        
        // x > -t and x < t
        for(int i = 0; i < vars; ++i)
        {
            // x - t < 0 
            row[i]       = 1;
            row[i+vars] = -1;
            solver.addConstraint(row, lpw::LE, 0);
            
            // x + t > 0
            row[i]       = 1;
            row[i+vars] =  1;
            solver.addConstraint(row, lpw::GE, 0);
            
            row[i] = row[i+vars] = 0;
        }
        
        // t   
        std::fill(row.begin(), row.end(), 0);
        for(int i = vars; i < 2*vars; ++i)
            row[i] = 1;
        
        solver.setObjective(row, false);
        
        double obj;
        solver.solve(row, obj);
        
        Eigen::VectorXd out(vars);
        
        for(int i = 0; i < vars; ++i)
            out(i) = row[i];
        
        return out;
    }
    
    // positive x only
    inline Eigen::VectorXd bp_p(const Eigen::MatrixXd& A, const Eigen::VectorXd& b)
    {
        const int vars = A.cols();
        
        lpw::Solver solver(vars);
        lpw::VD row = solver.makeRow();
        
        for(int i = 0; i < A.rows(); ++i)
        {
            for(int j = 0; j < vars; ++j)
                row[j] = A(i,j);
            
            solver.addConstraint(row, lpw::EQ, b(i));
        }
        
        for(int i = 0; i < vars; ++i)
            row[i] = 1;
        
        solver.setObjective(row, false);
        
        double obj;
        solver.solve(row, obj);
        
        Eigen::VectorXd out(vars);
        
        for(int i = 0; i < vars; ++i)
            out(i) = row[i];
        
        return out;
    }
};

#endif
