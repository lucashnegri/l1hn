#ifndef L1HN_FOCUSS
#define L1HN_FOCUSS

/* Implements the Adaptive Regularized FOCUSS Algorithm [1].
 * 
 * [1] Xia, T. Q., Zheng, Y., Wan, Q., & Wang, X. G. (2007).
 *     Adaptive Regularized FOCUSS Algorithm.
 *     In 2007 IEEE Radar Conference (pp. 282–284). IEEE.
 *     doi:10.1109/RADAR.2007.374228
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include "utils.hpp"

namespace l1hn
{   
    inline Eigen::VectorXd focuss(const Eigen::MatrixXd& A, const Eigen::VectorXd& b,
        const double p = 0.9, const double lambda = 0.05, const int k = 20)
    {
        const Eigen::MatrixXd Ah = A.adjoint();
        Eigen::VectorXd x  = pinv(A) * b; // initial solution
        Eigen::VectorXd w  = Eigen::MatrixXd::Ones(x.rows(), 1);
        
        for(int it = 0; it < k; ++it)
        {   
            const Eigen::MatrixXd W  = x.array().abs().pow(p).matrix().asDiagonal();
            const Eigen::MatrixXd Wh = W.adjoint();
            
            const double max = x.array().abs().maxCoeff();
            w = (1.0 - (x.array().abs() / max) - 0.1).matrix().cwiseProduct(w);
            x = W * (Wh * Ah * A * W + lambda * (Eigen::MatrixXd)w.asDiagonal()).inverse() * Wh * Ah * b;
        }
        
        return x;
    }
}

#endif
