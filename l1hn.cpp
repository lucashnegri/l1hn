#include <iostream>
#include <iomanip>
#include <string>

#include "amp.hpp"
#include "sl0.hpp"
#include "omp.hpp"
#include "sgrad.hpp"
#include "bp.hpp"
#include "focuss.hpp"

using namespace Eigen;
using namespace std;

int main(int argc, char* argv[])
{   
    if(argc != 4)
    {
        cout << "Usage: " << argv[0] << " A b x0" << endl;
        return 0;
    }
    
    /* load input */
    MatrixXd A  = l1hn::load_matrix(argv[1]);
    VectorXd b  = l1hn::load_vector(argv[2]);
    VectorXd x0 = l1hn::load_vector(argv[3]);
    
    if(A.rows() != b.rows() || A.cols() != x0.rows())
    {
        cout << "Invalid matrix sizes" << endl;
        return 0;
    }
    
    /* recover the original signal */
    try
    {
        VectorXd x = l1hn::focuss(A, b);
        
        /* print computed SNR */
        cout << "SNR: " << setprecision(5) << l1hn::SNR(x0, x) << " dB" << endl;
        l1hn::save_vector("x", x);
    } catch(const string& str)
    {
        cout << str << endl;
    }
    
    return 0;
}
