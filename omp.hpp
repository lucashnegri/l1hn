#ifndef L1HN_OMP
#define L1HN_OMP

/* Implements the orthogonal matching pursuit [1] algorithm.
 * [1]: Tropp, J. et al (2007). Signal Recovery From Random Measurements Via Orthogonal Matching Pursuit.
 *      doi:10.1109/TIT.2007.909108.
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include "utils.hpp"

namespace l1hn
{   
    inline Eigen::VectorXd omp(const Eigen::MatrixXd& A, const Eigen::VectorXd& b, int k)
    {
        const Eigen::MatrixXd At = A.transpose();
        Eigen::VectorXd r = b; // residual
        std::vector<int> Lambda;    // positions of the non-zero elements of x
        
        int it = 0;
        while(true)
        {
            // select the column of A that is most correlated to the residual
            Eigen::VectorXd u = (At*r).cwiseAbs();
            const int idx = max_idx(u);
            Lambda.push_back(idx);
            
            // solve the partial system to find the amplitudes of the already selected basis
            const Eigen::MatrixXd An = sub_matrix(A, Lambda);
            Eigen::VectorXd xp = An.colPivHouseholderQr().solve(b);
            r = An*xp - b;
            ++it;
            
            // end of the procedure
            if(it == k)
            {
                Eigen::VectorXd x = Eigen::MatrixXd::Zero(A.cols(), 1);
                
                for(int i = 0, e = Lambda.size(); i < e; ++i)
                    x[Lambda[i]] = xp[i];
                
                return x;
            }
        }
        // never reached
    }
};

#endif
