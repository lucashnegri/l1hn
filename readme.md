# About l1hn

Implements some algorithms related to compressed sensing.

## Dependencies

eigen3 (compile-time only), lpw (compile and runtime).

## License

LGPL.

## Contact

Lucas Hermann Negri - lucashnegri@gmail.com
