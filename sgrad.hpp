#ifndef L1HN_SGRAD
#define L1HN_SGRAD

/* Implements the subgradient algorithm.
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include "utils.hpp"

namespace l1hn
{   
    // only for positive x_i
    inline Eigen::VectorXd sgrad(const Eigen::MatrixXd& A, const Eigen::VectorXd& b, double alpha_0 = 0.1,
            double eps = 1e-9, unsigned max_it = 1e6, unsigned max_stuck = 5000
    )
    {
        // helpers
        const Eigen::MatrixXd A_pinv  = pinv(A);
        const Eigen::MatrixXd I       = Eigen::MatrixXd::Identity(A.cols(), A.cols());
        const Eigen::MatrixXd P = I - A_pinv*A; // projection
        
        Eigen::VectorXd x = A_pinv * b; // initial solution
        Eigen::VectorXd g = Eigen::VectorXd::Zero(A.cols(), 1);
        
        Eigen::VectorXd best_x = x;
        double best_l1 = 1e99;
        unsigned stuck = 0;
        
        for(unsigned it = 0; it < max_it; ++it)
        {
            // compute the gradient
            for(int i = 0, e = g.rows(); i < e; ++i)
                g[i] = x[i] >= 0 ? 1 : -1;
            
            // step
            const double alpha = alpha_0 / g.norm();
            x -= alpha*P*g;
            
            // stoping condition
            double l1 = 0;
            for(int i = 0, e = x.rows(); i < e; ++i)
                l1 += std::abs(x[i]);
            
            // track the best
            if(l1 < best_l1)
            {
                best_x  = x;
                best_l1 = l1;
            }
            else
                if(++stuck > max_stuck) break;
        };
        
        return best_x;
    }
};

#endif
