#ifndef L1HN_SL0
#define L1HN_SL0

/* Implements the smoothed L0 minimization [1] algorithm.
 * [1]: Mohimani, G. H. et. al. (2007). Fast Sparse Representation Based on Smoothed 0 Norm.
 * 
 * Lucas Hermann Negri, 2014. 
 */

#include <Eigen/Dense>
#include "utils.hpp"

namespace l1hn
{
    inline Eigen::VectorXd sl0_delta(const Eigen::VectorXd& x, double sigma)
    {
        return x.array() * ( -(x.array().abs2()) / (2*sigma*sigma) ).exp();
    }
    
    inline Eigen::VectorXd sl0(const Eigen::MatrixXd& A, const Eigen::VectorXd& b,
        double sigma_min = 1e-6, int L = 3, double sd = 0.8, double mu = 2.5)
    {
        const Eigen::MatrixXd A_pinv = pinv(A);
        Eigen::VectorXd x      = A_pinv * b;
        double sigma = 2 * x.cwiseAbs().maxCoeff();
        
        while(sigma > sigma_min)
        {
            for(int i = 0; i < L; ++i)
            {
                Eigen::VectorXd delta = sl0_delta(x, sigma);
                x -= mu * delta;
                x -= A_pinv*(A*x - b);
            }
        
            sigma *= sd;
        }
        
        return x;
    }
};

#endif
