#ifndef L1HN_UTILS
#define L1HN_UTILS

#include <fstream>
#include <cmath>
#include <Eigen/Dense>

namespace l1hn
{
    inline Eigen::MatrixXd load_matrix(const char* path)
    {
        unsigned m, n;
        std::ifstream inp;
        inp.open(path);
        inp >> m >> n;
        
        Eigen::MatrixXd mat(m,n);
        for(unsigned i = 0; i < m; ++i)
            for(unsigned j = 0; j < n; ++j)
                inp >> mat(i,j);
            
        return mat;
    }
    
    inline Eigen::MatrixXd load_vector(const char* path)
    {
        unsigned m;
        std::ifstream inp;
        inp.open(path);
        inp >> m;
        
        Eigen::VectorXd vec(m);
        for(unsigned i = 0; i < m; ++i)
            inp >> vec(i);
            
        return vec;
    }
    
    inline void save_vector(const char* path, const Eigen::VectorXd& vec)
    {
        std::ofstream of;
        of.open(path);
        
        of << vec.rows() << std::endl;
    
        for(int i = 0, e = vec.rows(); i < e; ++i)
            of << vec(i) << std::endl;
    }
    
    inline double SNR(Eigen::VectorXd& sig, Eigen::VectorXd& est)
    {
        double signal = sig.squaredNorm();
        double noise  = (est-sig).squaredNorm();
        return 10 * std::log10(signal / noise);
    }
    
    inline Eigen::MatrixXd pinv(const Eigen::MatrixXd& a, double epsilon = std::numeric_limits<double>::epsilon() )
    {
        Eigen::JacobiSVD<Eigen::MatrixXd> svd(a, Eigen::ComputeThinU | Eigen::ComputeThinV);
        double tolerance = epsilon * std::max( a.cols(), a.rows() ) * svd.singularValues().array().abs()(0);
        
        return svd.matrixV() * (svd.singularValues().array().abs() > tolerance)
                .select(svd.singularValues().array().inverse(),0).matrix().asDiagonal() * svd.matrixU().adjoint();
    }
    
    inline int max_idx(const Eigen::VectorXd& v)
    {
        double m_val = v[0];
        int m_idx    = 0;
        
        for(int i = 1, e = v.size(); i < e; ++i)
        {
            if(v[i] > m_val)
            {
                m_idx = i;
                m_val = v[i];
            }
        }
        
        return m_idx;
    }
    
    inline Eigen::MatrixXd sub_matrix(const Eigen::MatrixXd& A, const std::vector<int>& idx)
    {
        Eigen::MatrixXd An( A.rows(), idx.size() );
        
        for(int i = 0, e = idx.size(); i < e; ++i)
            An.col(i) = A.col(idx[i]);
        
        return An;
    }
};

#endif
